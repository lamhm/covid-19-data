# Covid-19 Data

This is a data repository of publicly available information of COVID-19 cases in Vietnam. Most of the data were aggregated from local press articles. This repository is maintained by a group of researchers and students at the Oxford University Clinical Research Unit (OUCRU) and the University of Medicine and Pharmacy (UMP) in Ho Chi Minh City, Vietnam.

## Data structrures

The data are stored in 3 csv files: 'Case Info.csv', 'Movement History.csv', and 'GPS Coordinates.csv'

### Case info

This dataset has the following columns:

* ID: public reported ID of cases.
* 'ID cũ' (old ID): old public reported ID of cases, only available when a F1 or F2 case became a positive case (F0).
* 'Cluster ID': only available when cases were within a transmission cluster.
* 'Giới tính' (gender): gender of cases.
* 'Tuổi' (age): age of cases (in years).
* 'Ngày xác nhận dương tính' (positive confirmation date): date when patient was formally confirmed as positive with SARS-CoV-2 (only available for F0). 
* 'Ngày phơi nhiễm' (date of exposure): suspected date when patient contacted with F0 case (YYYYMMDD).
* 'Ngày nhập cảnh' (date of importation): date when an imported F0 case entered the Vietnamese border (YYYYMMDD) (not available for F1, F2 or locally-transmitted F0)
* 'Ngày bị cách ly' (date of quarantine/isolation): date when patients were quarantine/isolated (YYYYMMDD).
* 'Số lượng F1' (number of F1): only available for F0 cases. Within a cluster, this information will be reported as 'total number of F1'/'total number of F0'. 
* 'Số lượng F2' (number of F2): only available for F0 cases. Within a cluster, this information will be reported as 'total number of F2'/'total number of F0'.
* 'Ghi chú' (notes): further notes for a patient.

### Movement History

Each row corresponds to a F0/F1/F2 case.

The first column represents public reported ID of cases. 

Next columns represent location (in word) of cases on each date. These columns were ordered by time.

### GPS Coordinates (to be entered)

Each row corresponds to a F0/F1/F2 case.

The first column represents public reported ID of cases. 

Next columns represent location (in GPS) of cases on each date. These columns were ordered by time.

## Maintenance

This data repository was set up and maintained by a group of researchers and students at OUCRU and UMP:

### Team at Oxford University Clinical Research Unit (OUCRU)

* Nguyễn Mạnh Duy
* Trịnh Mạnh Hùng
* Phan Nguyễn Quốc Khánh
* Hạ Minh Lâm
* Huỳnh Thị Phương
* Nguyễn Thị Lệ Thanh
* Đặng Duy Hoàng Giang

### Team at University of Medicine and Pharmacy at Ho Chi Minh City (UMP)

* Nguyễn Thị Hồng Diễm
* Nguyễn Linh Đan
* Thạch Ngọc Ét China
* Nguyễn Thị Hoàng Dung
* Trần Hồ Vĩnh Lộc
* Đinh Nguyễn Hoài Trang
* Lê Diệp Thanh Trang
* Phạm Tú Quyên
* Nguyễn Đỗ Phương Thảo
* Lý Thùy Đan Phương
* Lê Võ Hồng Tuyết

